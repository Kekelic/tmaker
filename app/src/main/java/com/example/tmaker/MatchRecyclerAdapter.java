package com.example.tmaker;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



public class MatchRecyclerAdapter extends RecyclerView.Adapter<MatchViewHolder> {

    private Tournament tournament = Tournament.getInstance();

    public MatchRecyclerAdapter( ){
    }

    @NonNull
    @Override
    public MatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cellView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_match, parent, false);
        return new MatchViewHolder(cellView);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchViewHolder holder, int position) {
        Team homeTeam = tournament.getMatches().get(position).getHomeTeam();
        Team awayTeam = tournament.getMatches().get(position).getAwayTeam();
        holder.setHomeTeam(homeTeam);
        holder.setAwayTeam(awayTeam);

        holder.etHomeScore.setText(tournament.getMatches().get(position).getHomeTeam().getScore());
        holder.etAwayScore.setText(tournament.getMatches().get(position).getAwayTeam().getScore());
    }

    @Override
    public int getItemCount() {
        return tournament.getMatches().size();
    }


}
