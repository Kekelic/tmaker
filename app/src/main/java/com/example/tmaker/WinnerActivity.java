package com.example.tmaker;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;


public class WinnerActivity extends AppCompatActivity {

    private Button bNewTournament;
    private TextView tvTournamentName, tvWinnerTeam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);
        setupText();
        setupButton();
    }

    private void setupText(){
        tvTournamentName =(TextView) findViewById(R.id.tvTournamentName);
        tvTournamentName.setText(Tournament.getInstance().getName());
        tvWinnerTeam = (TextView) findViewById(R.id.tvWinnerTeam);
        tvWinnerTeam.setText(Tournament.getInstance().getMatches().get(0).getHomeTeam().getName());
    }

    private void setupButton(){
        bNewTournament=(Button) findViewById(R.id.bNewTournament);
        bNewTournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tournament.getInstance().getMatches().clear();
                Tournament.getInstance().getGroups().clear();
                Tournament.getInstance().resetRoundCounter();
                openHomeActivity();
            }
        });
    }

    public void openHomeActivity(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
