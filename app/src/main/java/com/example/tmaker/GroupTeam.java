package com.example.tmaker;

public class GroupTeam {
    private String name;
    private int points=0;

    public GroupTeam(String name){
        this.name=name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    public int getPoints() {
        return points;
    }
}
