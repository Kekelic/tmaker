package com.example.tmaker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class TeamRecyclerAdapter extends RecyclerView.Adapter<TeamViewHolder>{

    private ArrayList<String> dataList = new ArrayList<>();
    private TeamClickListener clickListener;

    public TeamRecyclerAdapter(TeamClickListener listener){this.clickListener=listener;}

    @NonNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cellView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_team,parent, false);
        return new TeamViewHolder(cellView, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewHolder holder, int position) {
        holder.setTeam(dataList.get(position));

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public void addNewCell(String name, int position){
        if(dataList.size()>=position){
            dataList.add(position, name);
            notifyItemInserted(position);
        }
    }

    public void removeCell(int position){
        if(dataList.size()>position && position>=0){
            dataList.remove(position);
            notifyItemRemoved(position);
        }
    }


    public ArrayList<String> getDataList(){
        return this.dataList;
    }
}
