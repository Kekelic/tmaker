package com.example.tmaker;

public class Match {
    private Team homeTeam, awayTeam;

    public Match(Team homeTeam, Team awayTeam){
        this.homeTeam=homeTeam;
        this.awayTeam=awayTeam;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

}
