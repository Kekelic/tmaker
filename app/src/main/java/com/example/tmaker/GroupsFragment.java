package com.example.tmaker;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GroupsFragment extends Fragment {

    private RecyclerView recycler;
    private GroupRecyclerAdapter adapter;
    private Button bCompleted;
    private TextView tvTournamentName;
    private static GroupsFragment instance;

    private GroupsFragment(){}

    public static GroupsFragment newInstance(){
        GroupsFragment fragment = new GroupsFragment();
        instance = fragment;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_results, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTournamentName = getView().findViewById(R.id.tvTournamentName);
        tvTournamentName.setVisibility(View.GONE);

        setupRecycler();
        setupButton();
    }


    private void setupRecycler(){
        recycler = getView().findViewById(R.id.recycleViewResults);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new GroupRecyclerAdapter();
        recycler.setAdapter(adapter);
    }

    private void setupButton(){
        bCompleted = getView().findViewById(R.id.bCompleted);
        bCompleted.setVisibility(View.GONE);
        bCompleted.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                for (int i = 0; i < Tournament.getInstance().getGroups().size(); i++) {
                    List<GroupTeam> teams = Tournament.getInstance().getGroups().get(i).getTeams();
                    Collections.sort(teams, new Comparator<GroupTeam>() {
                        @Override
                        public int compare(GroupTeam o1, GroupTeam o2) {
                            return Integer.compare(o1.getPoints(), o2.getPoints());
                        }
                    });
                    Collections.reverse(teams);
                }
                Tournament.getInstance().setMatches(Tournament.getInstance().getTeamsForPlayoff());
                openResultsActivity();
            }
        });
    }

    private void openResultsActivity(){
        Intent intent =new Intent(getActivity(), ResultsActivity.class);
        startActivity(intent);
    }

    public GroupRecyclerAdapter getAdapter(){
        return adapter;
    }

    public static GroupsFragment getInstance(){
        return instance;
    }

    public Button getButton(){
        return bCompleted;
    }
}
