package com.example.tmaker;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private List<GroupTeam>  groupTeams = new ArrayList<>();

    public void addTeam(GroupTeam groupTeam){
        groupTeams.add(groupTeam);
    }

    public GroupTeam getTeam(int i){
        return groupTeams.get(i);
    }

    public List<GroupTeam> getTeams(){
        return groupTeams;
    }

}
