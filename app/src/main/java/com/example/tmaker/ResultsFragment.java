package com.example.tmaker;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

public class ResultsFragment extends Fragment {

    private static final String message = "You have to set all results";
    private RecyclerView recycler;
    private MatchRecyclerAdapter adapter;
    private Button bCompleted;
    private TextView tvTournamentName;

    public static ResultsFragment newInstance(){
        ResultsFragment fragment = new ResultsFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_results, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTournamentName= view.findViewById(R.id.tvTournamentName);
        tvTournamentName.setVisibility(View.GONE);

        setupRecycler();
        setupButton();
    }

    private void setupRecycler(){
        recycler = getView().findViewById(R.id.recycleViewResults);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MatchRecyclerAdapter();
        recycler.setAdapter(adapter);
    }

    private void setupButton(){
        bCompleted = getView().findViewById(R.id.bCompleted);
        bCompleted.setText("Update");
        bCompleted.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                Tournament tournament= Tournament.getInstance();
                List<Match> matches = tournament.getMatches();
                for(int i=0; i<matches.size();i++){
                    if (TextUtils.isEmpty(matches.get(i).getHomeTeam().getScore())||TextUtils.isEmpty(matches.get(i).getAwayTeam().getScore())){
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if(Tournament.getInstance().getRoundCounter()==3){
                    GroupsFragment.getInstance().getButton().setVisibility(View.VISIBLE);
                }

                tournament.updatePoints();
                tournament.makeNewRound();

                adapter.notifyDataSetChanged();

                GroupsFragment.getInstance().getAdapter().notifyDataSetChanged();

                ViewPager mViewPager;
                mViewPager = getActivity().findViewById(R.id.viewPager);
                mViewPager.setCurrentItem(0);

            }

        });
    }

}
