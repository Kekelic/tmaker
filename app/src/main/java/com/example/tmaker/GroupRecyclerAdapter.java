package com.example.tmaker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GroupRecyclerAdapter extends RecyclerView.Adapter<GroupViewHolder> {

    private Tournament tournament = Tournament.getInstance();

    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cellView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_table,parent, false);
        return new GroupViewHolder(cellView);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder holder, int position) {
        List<GroupTeam> groupTeams = new ArrayList<>(tournament.getGroups().get(position).getTeams());


        Collections.sort(groupTeams, new Comparator<GroupTeam>() {
            @Override
            public int compare(GroupTeam o1, GroupTeam o2) {
                return Integer.compare(o1.getPoints(), o2.getPoints());
            }
        });
        Collections.reverse(groupTeams);

        holder.setTeam1(groupTeams.get(0).getName());
        holder.setTeam2(groupTeams.get(1).getName());
        holder.setTeam3(groupTeams.get(2).getName());
        holder.setTeam4(groupTeams.get(3).getName());

        holder.setPts1(groupTeams.get(0).getPoints());
        holder.setPts2(groupTeams.get(1).getPoints());
        holder.setPts3(groupTeams.get(2).getPoints());
        holder.setPts4(groupTeams.get(3).getPoints());



    }

    @Override
    public int getItemCount() {
        return tournament.getGroups().size();
    }

}
