package com.example.tmaker;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class AddTeamActivity  extends AppCompatActivity implements TeamClickListener {

    private static final String messageNotTeamsForGroup = "You don't have the appropriate number of teams for the group stage, add or delete so that it is divisible by 4";
    private static final String messageNotTeamsForPlayoff ="You don't have enough teams for the playoff";
    private RecyclerView recycler;
    private TeamRecyclerAdapter adapter;
    private EditText etAddTeam;
    private Button bNext;
    private RadioButton rbGroup, rbPlayoff;
    private TextView tvNumberOfTeams;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team);
        setupRecycler();
        etAddTeam = (EditText) findViewById(R.id.etAddTeam);
        rbGroup = (RadioButton) findViewById(R.id.rbGroup);
        rbPlayoff = (RadioButton) findViewById(R.id.rbPlayoff);
        tvNumberOfTeams = (TextView) findViewById(R.id.tvNumberOfTeams);
        setupButton();
    }


    private void setupRecycler(){
        recycler = findViewById(R.id.recycleViewTeam);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TeamRecyclerAdapter(this);
        recycler.setAdapter(adapter);
    }

    public void addCellTeam(View view){
        if(!TextUtils.isEmpty(etAddTeam.getText().toString().trim())) {
            adapter.addNewCell(etAddTeam.getText().toString().trim(), 0);
            recycler.scrollToPosition(0);
            tvNumberOfTeams.setText(adapter.getItemCount() + " Teams");
            etAddTeam.getText().clear();
        }
    }

    @Override
    public void onDeleteClick(int position) {
        adapter.removeCell(position);
        tvNumberOfTeams.setText(adapter.getItemCount() +" Teams");
    }


    private void setupButton(){
        bNext = (Button) findViewById(R.id.bNext);
        bNext.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (rbPlayoff.isChecked()) {
                    if (adapter.getItemCount()>1) {
                        Tournament.getInstance().setMatches(adapter.getDataList());
                        openResultsActivity();
                    }
                    else {
                        Toast.makeText(getBaseContext(), messageNotTeamsForPlayoff, Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if(adapter.getItemCount()%4==0 && adapter.getItemCount()!=0) {
                        Tournament.getInstance().setGroups(adapter.getDataList());
                        Tournament.getInstance().makeNewRound();
                        openGroupPhaseActivity();
                    }
                    else {
                        Toast.makeText(getBaseContext(), messageNotTeamsForGroup , Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void openResultsActivity(){
        Intent intent =new Intent(this, ResultsActivity.class);
        startActivity(intent);
    }

    private void openGroupPhaseActivity(){
        Intent intent = new Intent(this, GroupPhaseActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
