package com.example.tmaker;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TeamViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView tvTeam;
    private Button bDelete;
    private TeamClickListener clickListener;

    public TeamViewHolder(@NonNull View itemView, TeamClickListener listener) {
        super(itemView);
        this.clickListener=listener;
        tvTeam = itemView.findViewById(R.id.tvTeam);
        bDelete = itemView.findViewById(R.id.bDelete);
        bDelete.setOnClickListener(this);
    }

    public void setTeam(String team){tvTeam.setText(team);}

    @Override
    public void onClick(View view) {
        clickListener.onDeleteClick(getAdapterPosition());
    }
}
