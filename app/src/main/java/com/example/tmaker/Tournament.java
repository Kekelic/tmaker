package com.example.tmaker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Tournament {
    private String name;
    private List<Match> matches;
    private List<Group> groups;
    private static Tournament instance;
    private int roundCounter=0;
    private static final int winPoints = 3;
    private static final int drawPoints = 1;

    private Tournament() {
        this.matches = new ArrayList<>();
        this.groups = new ArrayList<>();

    }

    public static Tournament getInstance() {
        if (instance == null)
            instance = new Tournament();
        return instance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setGroups(List<String> teams){
        Collections.shuffle(teams);
        int i=0;
        while(i < teams.size()){
            groups.add(new Group());
            groups.get(i/4).addTeam(new GroupTeam(teams.get(i)));
            groups.get(i/4).addTeam(new GroupTeam(teams.get(i+1)));
            groups.get(i/4).addTeam(new GroupTeam(teams.get(i+2)));
            groups.get(i/4).addTeam(new GroupTeam(teams.get(i+3)));
            i+=4;
        }

    }

    public void setMatches(List<String> teams) {
        if (roundCounter==0)
            Collections.shuffle(teams);
        for (int i = 0; i < teams.size(); i++) {
            if (teams.size() % 2 == 1 & i == teams.size() - 1) {
                matches.add(new Match(new Team(teams.get(i)), new Team("NO TEAM")));
            } else if (i % 2 == 0)
                matches.add(new Match(new Team(teams.get(i)), new Team(teams.get(i + 1))));
        }
    }


    public void makeNewRound(){
        matches.clear();
        roundCounter++;
        if(roundCounter==1) {
            for (int i = 0; i < groups.size(); i++) {
                matches.add(new Match(new Team(groups.get(i).getTeam(0).getName()), new Team(groups.get(i).getTeam(1).getName())));
                matches.add(new Match(new Team(groups.get(i).getTeam(2).getName()), new Team(groups.get(i).getTeam(3).getName())));
            }
        }
        else if(roundCounter==2){
            for (int i = 0; i < groups.size(); i++) {
                matches.add(new Match(new Team(groups.get(i).getTeam(0).getName()), new Team(groups.get(i).getTeam(2).getName())));
                matches.add(new Match(new Team(groups.get(i).getTeam(1).getName()), new Team(groups.get(i).getTeam(3).getName())));
            }
        }
        else if(roundCounter==3){
            for (int i = 0; i < groups.size(); i++) {
                matches.add(new Match(new Team(groups.get(i).getTeam(0).getName()), new Team(groups.get(i).getTeam(3).getName())));
                matches.add(new Match(new Team(groups.get(i).getTeam(1).getName()), new Team(groups.get(i).getTeam(2).getName())));
            }
        }
    }

    public void updatePoints(){

        for(int i = 0; i < matches.size(); i++){
            if(roundCounter==1){
                if (i%2==0) {
                    if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) > Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(0).addPoints(winPoints);
                    } else if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) < Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(1).addPoints(winPoints);
                    } else {
                        groups.get(i / 2).getTeam(0).addPoints(drawPoints);
                        groups.get(i / 2).getTeam(1).addPoints(drawPoints);
                    }
                }
                else{
                    if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) > Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(2).addPoints(winPoints);
                    } else if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) < Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(3).addPoints(winPoints);
                    } else {
                        groups.get(i / 2).getTeam(2).addPoints(drawPoints);
                        groups.get(i / 2).getTeam(3).addPoints(drawPoints);
                    }
                }
            }
            else if(roundCounter==2){
                if (i%2==0) {
                    if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) > Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(0).addPoints(winPoints);
                    } else if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) < Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(2).addPoints(winPoints);
                    } else {
                        groups.get(i / 2).getTeam(0).addPoints(drawPoints);
                        groups.get(i / 2).getTeam(2).addPoints(drawPoints);
                    }
                }
                else{
                    if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) > Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(1).addPoints(winPoints);
                    } else if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) < Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(3).addPoints(winPoints);
                    } else {
                        groups.get(i / 2).getTeam(1).addPoints(drawPoints);
                        groups.get(i / 2).getTeam(3).addPoints(drawPoints);
                    }
                }
            }
            else if(roundCounter==3){
                if (i%2==0) {
                    if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) > Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(0).addPoints(winPoints);
                    } else if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) < Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(3).addPoints(winPoints);
                    } else {
                        groups.get(i / 2).getTeam(0).addPoints(drawPoints);
                        groups.get(i / 2).getTeam(3).addPoints(drawPoints);
                    }
                }
                else{
                    if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) > Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(1).addPoints(winPoints);
                    } else if (Integer.parseInt(matches.get(i).getHomeTeam().getScore()) < Integer.parseInt(matches.get(i).getAwayTeam().getScore())) {
                        groups.get(i / 2).getTeam(2).addPoints(winPoints);
                    } else {
                        groups.get(i / 2).getTeam(1).addPoints(drawPoints);
                        groups.get(i / 2).getTeam(2).addPoints(drawPoints);
                    }
                }
            }
        }
    }

    public List<Group> getGroups(){return groups;}

    public List<Match> getMatches() {
        return matches;
    }


    public void calculateMatches(){
        List<String> teams = new ArrayList<>();
        for(int i=0; i < matches.size();i++){
            Match match=matches.get(i);
            if(Integer.parseInt(match.getHomeTeam().getScore()) > Integer.parseInt(match.getAwayTeam().getScore()))
                teams.add(match.getHomeTeam().getName());
            else
                teams.add(match.getAwayTeam().getName());
        }
        matches.clear();
        instance.setMatches(teams);
    }

    public boolean isLast(){
        if(matches.size()==1 & matches.get(0).getAwayTeam().getName()=="NO TEAM")
            return true;
        else
            return false;
    }

    public int getRoundCounter() {
        return roundCounter;
    }
    public void resetRoundCounter(){
        roundCounter=0;
    }


    public List<String> getTeamsForPlayoff(){
        List<String> firstTeams = getFirstOfGroups();
        List<String> secondTeams = getSecondOfGroups();
        List<String> teams= new ArrayList<>();
        for(int i=0; i<firstTeams.size();i++){
            teams.add(firstTeams.get(i));
            teams.add(secondTeams.get(i));
        }
        return teams;
    }


    private List<String> getFirstOfGroups(){
        List<String> names = new ArrayList<>();
        for (int i=0; i<groups.size();i++){
            names.add(groups.get(i).getTeam(0).getName());
        }
        Collections.shuffle(names);
        return names;
    }

    private List<String> getSecondOfGroups(){
        List<String> names = new ArrayList<>();
        for (int i=0; i<groups.size();i++){
            names.add(groups.get(i).getTeam(1).getName());
        }
        Collections.shuffle(names);
        return names;
    }

}
