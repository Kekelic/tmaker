package com.example.tmaker;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MatchViewHolder extends RecyclerView.ViewHolder {

    private static final String message ="The result is a draw, the away team goes further";
    private TextView tvHomeTeam, tvAwayTeam;
    protected EditText etHomeScore, etAwayScore;


    public MatchViewHolder(@NonNull View itemView) {
        super(itemView);
        tvHomeTeam = itemView.findViewById(R.id.tvHomeTeam);
        tvAwayTeam = itemView.findViewById(R.id.tvAwayTeam);
        etHomeScore = itemView.findViewById(R.id.etHomeScore);
        etAwayScore = itemView.findViewById(R.id.etAwayScore);

        etHomeScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Match match = Tournament.getInstance().getMatches().get(getAdapterPosition());
                match.getHomeTeam().setScore(etHomeScore.getText().toString());
                if(Tournament.getInstance().getRoundCounter()==0 || Tournament.getInstance().getRoundCounter()>3)
                    if(!TextUtils.isEmpty(match.getHomeTeam().getScore()) && !TextUtils.isEmpty(match.getAwayTeam().getScore()))
                        if(Integer.parseInt(match.getHomeTeam().getScore())==Integer.parseInt(match.getAwayTeam().getScore()))
                            Toast.makeText(itemView.getContext(), message , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        etAwayScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Match match = Tournament.getInstance().getMatches().get(getAdapterPosition());
                match.getAwayTeam().setScore(etAwayScore.getText().toString());
                if(Tournament.getInstance().getRoundCounter()==0 || Tournament.getInstance().getRoundCounter()>3)
                    if(!TextUtils.isEmpty(match.getHomeTeam().getScore()) && !TextUtils.isEmpty(match.getAwayTeam().getScore()))
                        if(Integer.parseInt(match.getHomeTeam().getScore())==Integer.parseInt(match.getAwayTeam().getScore()))
                            Toast.makeText(itemView.getContext(), message , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    public void setHomeTeam(Team team){
        tvHomeTeam.setText(team.getName());}

    public  void setAwayTeam(Team team){
        tvAwayTeam.setText(team.getName());}
}
