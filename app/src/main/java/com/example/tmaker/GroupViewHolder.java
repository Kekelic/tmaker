package com.example.tmaker;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GroupViewHolder extends RecyclerView.ViewHolder {
    private TextView tvTeam1, tvTeam2, tvTeam3, tvTeam4;
    private TextView tvPts1, tvPts2, tvPts3, tvPts4;


    public GroupViewHolder(@NonNull View itemView) {
        super(itemView);
        tvTeam1 = itemView.findViewById(R.id.tvTeam1);
        tvTeam2 = itemView.findViewById(R.id.tvTeam2);
        tvTeam3 = itemView.findViewById(R.id.tvTeam3);
        tvTeam4 = itemView.findViewById(R.id.tvTeam4);
        tvPts1 = itemView.findViewById(R.id.tvPts1);
        tvPts2 = itemView.findViewById(R.id.tvPts2);
        tvPts3 = itemView.findViewById(R.id.tvPts3);
        tvPts4 = itemView.findViewById(R.id.tvPts4);
    }

    public void setTeam1(String team1) {
        this.tvTeam1.setText(team1);
    }

    public void setTeam2(String team2) {
        this.tvTeam2.setText(team2);
    }

    public void setTeam3(String team3) {
        this.tvTeam3.setText(team3);
    }

    public void setTeam4(String team4) {
        this.tvTeam4.setText(team4);
    }

    public void setPts1(int pts) { this.tvPts1.setText(pts +" pts"); }

    public void setPts2(int pts) {
        this.tvPts2.setText(pts +" pts");
    }

    public void setPts3(int pts) {
        this.tvPts3.setText(pts +" pts");
    }

    public void setPts4(int pts) {
        this.tvPts4.setText(pts +" pts");
    }
}
