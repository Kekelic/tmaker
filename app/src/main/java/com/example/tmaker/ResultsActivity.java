package com.example.tmaker;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    private static final String message = "You have to set all results";
    private RecyclerView recycler;
    private MatchRecyclerAdapter adapter;
    private Button bCompleted;
    private TextView tvTournamentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        tvTournamentName= (TextView) findViewById(R.id.tvTournamentName);
        tvTournamentName.setText(Tournament.getInstance().getName());
        setupRecycler();
        setupButton();

    }

    private void setupRecycler(){
        recycler = findViewById(R.id.recycleViewResults);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MatchRecyclerAdapter();
        recycler.setAdapter(adapter);
    }

    private void setupButton(){
        bCompleted = (Button) findViewById(R.id.bCompleted);
        bCompleted.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                List<Match> matches =Tournament.getInstance().getMatches();
                for(int i=0; i<matches.size();i++){
                    if (TextUtils.isEmpty(matches.get(i).getHomeTeam().getScore())||TextUtils.isEmpty(matches.get(i).getAwayTeam().getScore())){
                        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                Tournament.getInstance().calculateMatches();
                if(Tournament.getInstance().isLast())
                    openWinnerActivity();
                else reOpenActivity();
            }

        });
    }

    private void reOpenActivity(){
        Intent intent =new Intent(this, ResultsActivity.class);
        startActivity(intent);
    }

    private void openWinnerActivity(){
        Intent intent = new Intent(this, WinnerActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
