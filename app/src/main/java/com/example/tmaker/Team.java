package com.example.tmaker;

public class Team {
    private String name;
    private String score;

    public Team(String name){
        this.name=name;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getName(){return name;}

    public String getScore(){
        return score;}
}
