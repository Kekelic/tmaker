package com.example.tmaker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class HomeActivity extends AppCompatActivity {

    private static final String message="You have to enter tournament name";
    private Button bCreate;
    private EditText etTournamentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        etTournamentName = (EditText) findViewById(R.id.etTournamentName);
        setupButton();
    }

    private void setupButton(){
        bCreate=(Button) findViewById(R.id.bCreate);
        bCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(etTournamentName.getText().toString().trim()) )
                    Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();
                else{
                    Tournament.getInstance().setName(etTournamentName.getText().toString().trim());
                    openAddTeamActivity();
                }
            }
        });
    }

    public void openAddTeamActivity(){
        Intent intent = new Intent(this, AddTeamActivity.class);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}